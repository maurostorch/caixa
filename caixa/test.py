import unittest
import caixa

# Here's our "unit tests".
class CaixaTests(unittest.TestCase):

    def testOne(self):
        self.assertEqual(caixa.withdraw(0),[])

    def testTwo(self):
        self.assertEqual(caixa.withdraw(0),[])

    def testThree(self):
        self.assertEqual(caixa.withdraw(30),[20,10])

    def testFour(self):
        self.assertEqual(caixa.withdraw(80),[50,20,10])

    def testFive(self):
        self.assertNotEqual(caixa.withdraw(91),[50,20,10])

def main():
    unittest.main()

if __name__ == '__main__':
    main()
