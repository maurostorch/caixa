def withdraw(value):
    coins=[100,50,20,10]
    result=[]
    while value>=10:
        for c in coins:
            if value/c>0:
                result.extend([c]*(value/c))
                value=value%c
                break
    return result
